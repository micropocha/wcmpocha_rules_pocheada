(async () => {
  class wcmPochaRulesPocheada extends HTMLElement {
    constructor() {
      super();
      this._activeRoundFlag = null  
    }

    get name () {
      return "pocheada"  
    }

    get activeRoundFlag (){
      return this._activeRoundFlag;
    }

    set activeRoundFlag (flag){
      this._activeRoundFlag = flag;
    }
    
    buttons (lastScore, lastPartial) {
      var unmultipliedScoreforHits = (hits) => {
        if (hits >= 0) return hits * 5 +10
          else return hits * 5
      }
      var scoreforHits = (hits) => {
        var unmultipliedScore = unmultipliedScoreforHits(hits);
        switch ( this._activeRoundFlag ) {
          case 'x2':
            return unmultipliedScore * 2;
          case 'x3':
            return unmultipliedScore *3;
          default:
            return unmultipliedScore
        }
      }
      var caption = (hits, deltaPoints) => {
        return `(${hits}) ${deltaPoints} => ${lastScore + deltaPoints}`
      } 
      var button = (hits) => {
        return {
          value: hits,
          caption : caption (hits, scoreforHits(hits))
        }
      };
      if(  lastPartial !== undefined && lastPartial !== null ){
        return [button(lastPartial-1), button(lastPartial+1)];
      }
      else{
        return [button(-1 ), button( 0 )];
      }
    }
    
    get roundFlags (){
      return ['x2','x3'];
    }

    dealAmount ( round, playerAmount ) {
      if(playerAmount == 4 ) {
        var sround = round-3;
        if (round < playerAmount) return 1;
        else if (sround < 10) return sround;
        else if (sround >=10 && sround <14) return 10;
        else if (sround >=14 && sround <23) return 23 - sround;
        else return 10;
      }
      else if(playerAmount == 3 ) {
        var sround = round-2;
        if (round < playerAmount) return 1;
        else if (sround < 9) return sround;
        else if (sround < (9+playerAmount)) return 9;
        else if (sround < 9 + playerAmount + 8) return 20 - sround;
        else return 9;
      }
      else return null;                
    }
    
    // Called when element is inserted in DOM
    connectedCallback() {
      window.addEventListener('activatedRoundFlag', e => {
        this.activeRoundFlag = e.detail.flag
      })   
    };

  }
 
  customElements.define('wcmpocha-rules-pocheada', wcmPochaRulesPocheada);
})();