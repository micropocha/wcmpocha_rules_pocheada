Description
==================================

this component contains the specific rules of a game of kind pocheada

## attributes (html):

## properties (js)

- name: display name of the rule
- buttons: array  of display name for buttons (initially 2) containing value and caption
- dealAmount: number of cards to deal 
- roundFlags: array of roundOptions
- activeRoundFlag: to active roundflag 

## events

## listens
- activatedRoundFlag (flag)


Run
=======================================

## run first time

```bash
npm install 
npm start
```

## test

```bash
npm test
```